"""
Monitors PSS's DB2Queues by searching patterns into logs.
Can notify by email if defined.
Writes to log file defined in LOG_FILE_NAME.
"""

import datetime
import json
from os import path
import sys

from .pssapi.cmdexe import cmd
from .pssapi.logger.logger import load_logger

logger = None
EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE_NAME = path.join(EXEC_DIR, 'logs', 'db2q_monitor.log')
CONF_FILE_NAME = path.join(EXEC_DIR, 'conf', 'conf.json')
DEFAULT_DB = "CLFYTOPP"

def start():
    """Load the logger object and print the initial time to log file."""
    global logger
    logger_fmtter = {
        'INFO': '%(message)s'
    }
    logger = load_logger(log_file_name=LOG_FILE_NAME, **logger_fmtter)
    logger.info('============================================================')
    logger.info(datetime.datetime.now())

def quit_script(error_message=None):
    """Print error_message and datetime into log file. Quit current script."""
    if error_message:
        logger.error(error_message)
    logger.info('Quiting now...')
    logger.info(datetime.datetime.now())
    logger.info('============================================================')
    exit()

def _get_json_data():
    """Return the configuration file content as Python dict.
    If file doesn't exist or is not a valid JSON file, print error and quit.
    """
    logger.info("Reading conf JSON file...")
    try:
        with open(CONF_FILE_NAME) as json_data_file:
            return json.load(json_data_file)
    except Exception as e:
        quit_script("Error loading conf file: {0}".format(str(e)))

def _update_json(json_data):
    """Update conf.json file with json_data. Quit script on error."""
    logger.info("Updating conf JSON file...")
    try:
        with open(CONF_FILE_NAME, 'w') as json_data_file:
            json_data_file.write(json.dumps(json_data, indent=4))
            logger.info("File updated.")
    except Exception as e:
        quit_script("Error updating conf JSON file: {0}".format(str(e)))

def _build_cmd(server, server_conf, log_pattern):
    """Create a command to:
        - connect to server
        - check if processes defined in server_conf are running
        - check the lines of log_pattern in logs file
    Return the entire command in one string.
    Quit script in case of exception.
    """
    logger.info("Building command for server: {0}".format(server))
    sshcmd = "ssh {0}".format(server)
    processcmd = ""
    try:
        for process in server_conf:
            log_file = server_conf[process]['log_file']
            db = server_conf[process]['db'] if 'db' in server_conf[process] \
                                            else DEFAULT_DB
            # check if process is running
            processcmd += "ps -ef | grep '{0} ' | grep -v grep && ".format(process)
            # change to log directory
            processcmd += "cd {0} && ".format(server_conf[process]['log_file_path'])
            patterns = server_conf[process]['patterns'].keys()
            for pattern in patterns:
                # create log pattern
                log_p = log_pattern.format(pattern=pattern, db=db)
                # grep log files
                processcmd += "grep '{0}' {1} | wc -l".\
                                format(log_p, log_file)
                # add hack to identify pattern in the final string
                processcmd += " | xargs -i echo '{{}}@{0}'".format(pattern)
                processcmd += (';' if patterns[-1] == pattern else ' && ')
    except Exception as e:
        quit_script("Error creating command for server {0}: {1}".\
                    format(server, str(e)))
    return "{0} \"{1}\"".format(sshcmd, processcmd)

def _identify_process(line, processes, pattern):
    """Return the process contained in line."""
    for process in processes:
        if line.rfind(pattern.format(process)) != -1:
            return process
    return None

def _log_values(server, process, destid, curhour, curval, prehour, preval):
    """Log some important values."""
    premesvals = ("Previous:\n\t\thour: {0}\n\t\t" + 
                "logs count: {1}").format(prehour, preval)
    curmesvals = ("Current:\n\t\thour: {0}\n\t\t" + 
                "logs count: {1}").format(curhour, curval)
    logger.info(("Previous and current values for 'destid'={0} in process " + 
                 "{1} on server {2}:\n\t{3}\n\t{4}").\
                format(destid, process, server, premesvals, curmesvals))

def _check_in_server(server, server_conf, log_pattern, process_pattern):
    """Connect to server and check db2queues log values in there.
    
    Keyword arguments:
    server -- Server to check values in.
    server_conf -- Conf part in conf.json file containing the processes and 
        destid (queries) to be checked out.
    log_pattern -- Pattern to search into the logs.
    process_pattern -- Pattern defined in conf.json; used to search 
        processes in server.
    
    Return specific conf for server (server_conf) and the message containing 
    the resume of processes/destids values that are probably stuck.
    """
    logger.info("Checking in server: {0}".format(server))
    command = _build_cmd(server, server_conf, log_pattern)
    try:
        logger.info("Executing command for server: {0}".format(server))
        (stdout, stderr) = cmd.cmdexec(command)
        logger.info("STDOUT from running command:\n{0}".format(stdout))
        if stderr:
            logger.warning("There was an error when executing command on " + 
                            "server {0}: {1}".format(server, stderr))
        message = ""
        message_details = ("------------------------------------------------" +
                           "\n\tServer: {0}\n\tProcess: {1}\n\tQuery: {2}\n")
        curprocess = ""
        processes = server_conf.keys()
        for line in stdout.strip().split('\n'):
            if line.strip():
                process = _identify_process(line, processes, process_pattern)
                if process:
                    curprocess = process
                    continue
                if line.rfind('@') != -1 and curprocess != process:
                    splitline = line.split('@')
                    curvalue = splitline[0]
                    pattern = splitline[1]
                    prvalue = server_conf[curprocess]['patterns'][pattern]['count']
                    prhour = server_conf[curprocess]['patterns'][pattern]['hour']
                    curhour = datetime.datetime.now().hour
                    _log_values(server, curprocess, pattern, 
                                curhour, curvalue, prhour, prvalue)
                    if curhour == prhour and prvalue == curvalue:
                        # log was not moving
                        message += message_details.format(server, curprocess, 
                                                          pattern)
                        logger.warning(("It seems query with 'destid'={0} in " +
                            "process {1} on server {2} was not moving, " + 
                            "according to previous and current values " + 
                            "(same values).").format(pattern, curprocess, server))
                    server_conf[curprocess]['patterns'][pattern]['count'] = curvalue
                    server_conf[curprocess]['patterns'][pattern]['hour'] = curhour
        return (server_conf, message)
    except Exception as e:
        logger.error("There was an exception when checking server {0}: {1}".\
                    format(server, str(e)))

def _notify(message, conf_json):
    """Grab notifying values from conf file and send message by email."""
    if message:
        try:
            from .pssapi.emailer import emailer
            subject = conf_json['notifying']['subject']
            sender = conf_json['notifying']['sender']
            dests = conf_json['notifying']['dests']
            body = conf_json['notifying']['body'].format(message)
            priority = conf_json['notifying']['high_priority']
            save_email = conf_json['notifying']['save_email']
            res = emailer.send_email(subject=subject, body=body, 
                                     sender=sender, dest=dests, 
                                     saveit=save_email, highpriority=priority)
            if res == "OK":
                logger.info("Email was sent to {0}".format(dests))
            else:
                logger.error("Error when trying to send email to {0}: {1}".\
                            format(dests, res))
        except Exception as e:
            quit_script("Error on notifying: {0}".format(str(e)))

def check_log():
    """Check logs for servers/processes/destids defined in conf file.
    Notify responsible teams.
    Update conf file with current values from logs.
    """
    conf_json = _get_json_data()
    logp = conf_json['log_pattern']
    processp = conf_json['process_pattern']
    res_message = ""
    for server in conf_json['servers'].keys():
        (upservconf, message) = _check_in_server(server, 
                                conf_json['servers'][server], logp, processp)
        conf_json['servers'][server] = upservconf
        if message:
            res_message = (res_message + message if res_message \
                            else message)
    _notify(res_message, conf_json)
    _update_json(conf_json)
