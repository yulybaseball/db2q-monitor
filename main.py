#!/bin/env python

def main():
    mon.start()
    mon.check_log()
    mon.quit_script()

if __name__ == "__main__":
    if __package__ is None:
        # for calling as a script. e.g.
        #   $ ./main
        #   $ python db2q_monitor/main.py
        from os import path
        import sys
        sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    # for calling as a package, too. e.g.
    #   $ python -m db2q_monitor.main
    from db2q_monitor import db2q_monitor as mon
    main()
